# arch-linux-notices

Web scraper that pulls important notices from https://www.archlinux.org. It utilizes enlive and http-kit. Currently pulls "Latest News" titles from the frontpage.

## Usage

To use:

1.  Clone this repo.
2.  Cd into the arch-linux-notices directory.
3.  Ensure you have Leiningen installed, and run

        lein run

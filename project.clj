(defproject arch-linux-notices "0.0.1"
  :description "Simply web scraper that pulls in important Arch Linux notices such as security advisories or update interventions."
  :url "https://prestonlibby.tech"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [enlive "1.1.6"]
                 [http-kit "2.4.0-alpha4"]]
  :main ^:skip-aot arch-linux-notices.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})

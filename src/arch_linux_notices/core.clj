(ns arch-linux-notices.core
  (:require [net.cgrand.enlive-html :as html]
            [org.httpkit.client :as http]))

(defn get-news-dom
  []
  (html/html-snippet
    (:body @(http/get "http://www.archlinux.org/" {:insecure? true}))))

(defn get-news-titles
  [dom]
  (map
    (comp first :content) (html/select dom [:h4 :a])))

(defn get-news-links
  [dom]
    (map :href
      (map :attrs (html/select dom [:h4 :a]))))

(defn -main
  []
  (run! println (get-news-titles (get-news-dom)))
  (run! println (get-news-links (get-news-dom))))
